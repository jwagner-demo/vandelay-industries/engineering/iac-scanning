#!/usr/bin/env python

# Info: Script to parse IaC security scanner results from kics
# and create a markdown table in a merge request comment
# License: MIT, Copyright (c) 2022-present GitLab B.V.
# Author: Michael Friedrich <mfriedrich@gitlab.com>

import os
import sys
import json
import gitlab

# Manual calls for development
# GITLAB_TOKEN needs to be sourced into the .env
#
# 1. Generate a kics report (or use `kics/kics-report.json in the repo`)
# $ kics scan --path kubernetes --report-formats json --output-path kics --output-name kics-report.json
#
# 2. Run on a project & MR to update
# $ CI_PROJECT_ID=31640278 CI_MERGE_REQUEST_ID=3 python3 ./integrations/kics-scan-report-mr-update.py
# $ CI_PROJECT_ID=31640278 CI_COMMIT_REF_NAME=test2 python3 ./integrations/kics-scan-report-mr-update.py

FILE="kics/kics-report.json" # TODO: parameter

if 'GITLAB_URL' in os.environ:
    GITLAB_URL = os.environ['GITLAB_URL']
else:
    GITLAB_URL = 'https://gitlab.com'

f = open(FILE)
report = json.load(f)

# Parse the report: kics
if "kics_version" in report:
    print("Found kics '%s' in '%s'" % (report["kics_version"], FILE))
    queries = report["queries"]
else:
    raise Exception("Unsupported report format")

comment_body = """### kics vulnerabilities report

| Severity | Description | Platform | Filename |
|----------|-------------|----------|----------|
"""

# Example query to parse: {'query_name': 'Service Does Not Target Pod', 'query_id': '3ca03a61-3249-4c16-8427-6f8e47dda729', 'query_url': 'https://kubernetes.io/docs/concepts/services-networking/service/', 'severity': 'LOW', 'platform': 'Kubernetes', 'category': 'Insecure Configurations', 'description': 'Service should Target a Pod', 'description_id': 'e7c26645', 'files': [{'file_name': 'kubernetes/ecc-demo-service.yml', 'similarity_id': '9da6166956ad0fcfb1dd533df17852342dcbcca02ac559becaf51f6efdc015e8', 'line': 38, 'issue_type': 'IncorrectValue', 'search_key': 'metadata.name={{ecc-demo-service}}.spec.ports.name={{web}}.targetPort', 'search_line': 0, 'search_value': '', 'expected_value': 'metadata.name={{ecc-demo-service}}.spec.ports={{web}}.targetPort has a Pod Port', 'actual_value': 'metadata.name={{ecc-demo-service}}.spec.ports={{web}}.targetPort does not have a Pod Port'}]} 

for q in queries:
    #print(q) # DEBUG
    l = []
    l.append(q["severity"])
    l.append(q["description"])
    l.append(q["platform"])

    if "files" in q:
        l.append(",".join((f["file_name"] for f in q["files"])))

    comment_body += "| " + " | ".join(l) + " |\n"

f.close()

#print(comment_body) # DEBUG

# Use the GitLab API to add a comment to the MR
# 1. instance https://python-gitlab.readthedocs.io/en/stable/api-usage.html 

# Note: This needs a dedicated API token. The CI_JOB_TOKEN is not sufficient.
# https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html 
if 'GITLAB_TOKEN' in os.environ:
    gl = gitlab.Gitlab(GITLAB_URL, private_token=os.environ['GITLAB_TOKEN'])
else:
    raise Exception('GITLAB_TOKEN variable not set. Please provide an API token to update the MR!')

# 2. project by id https://python-gitlab.readthedocs.io/en/stable/gl_objects/projects.html
project = gl.projects.get(os.environ['CI_PROJECT_ID'])

# 3. MR by id https://python-gitlab.readthedocs.io/en/stable/gl_objects/merge_requests.html
# CI_MERGE_REQUEST_ID is not always set. Try a workaround with the source branch
#if not 'CI_MERGE_REQUEST_ID' in os.environ:
#    raise Exception("Script needs to be run in a Merge Request pipeline")
real_mr = None

if 'CI_MERGE_REQUEST_ID' in os.environ:
    mr_id = os.environ['CI_MERGE_REQUEST_ID']
    real_mr = project.mergerequests.get(mr_id)

# Note: This workaround can be very expensive in projects with many MRs
if 'CI_COMMIT_REF_NAME' in os.environ:
    commit_ref_name = os.environ['CI_COMMIT_REF_NAME']

    mrs = project.mergerequests.list()

    for mr in mrs:
        if mr.source_branch in commit_ref_name:
            real_mr = mr
            # found the MR for this source branch
            # print(mr) # DEBUG

if not real_mr:
    print("Pipeline not run in a merge request, no reports sent")
    sys.exit(0)

# 4. notes to MR https://python-gitlab.readthedocs.io/en/stable/gl_objects/notes.html
# print(real_mr) # DEBUG
mr_note = real_mr.notes.create({'body': comment_body})

# TODO: Check if a report already exists; avoid spamming the MR comments.
#for n in real_mr.notes.list():
#    print(n)
